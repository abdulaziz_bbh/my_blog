package com.example.my_blog.valid;

import com.example.my_blog.dto.UserRegDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        UserRegDto userRegDto = (UserRegDto) object;
        return userRegDto.getPassword().equals(userRegDto.getMatchingPassword());
    }
}
