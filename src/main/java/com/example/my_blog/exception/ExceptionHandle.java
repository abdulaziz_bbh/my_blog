package com.example.my_blog.exception;

import com.example.my_blog.component.MessageService;
import com.example.my_blog.response.ErrorData;
import com.example.my_blog.response.ResponseData;
import com.example.my_blog.utils.RestConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class ExceptionHandle {

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(MethodArgumentNotValidException exception) {
        List<ErrorData> errorData = new ArrayList<>();
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        fieldErrors.forEach(fieldError -> errorData.add(new ErrorData(fieldError.getDefaultMessage(), fieldError.getField(), RestConstants.REQUIRED)));
        return new ResponseEntity<>(ResponseData.errorResponse(errorData), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {HttpMessageNotReadableException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(HttpMessageNotReadableException exception) {
        return new ResponseEntity<>(ResponseData.errorResponse(exception.getMessage(), RestConstants.CONFLICT), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {TypeNotPresentException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(TypeNotPresentException exception) {
        return new ResponseEntity<>(ResponseData.errorResponse(exception.getMessage(), 400), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(MissingServletRequestParameterException ex) {
        return new ResponseEntity<>(ResponseData.errorResponse(ex.getMessage(), 400), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {ServletRequestBindingException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(ServletRequestBindingException ex) {
        return new ResponseEntity<>(ResponseData.errorResponse(ex.getMessage(), 400), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MissingServletRequestPartException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(MissingServletRequestPartException ex) {
        return new ResponseEntity<>(ResponseData.errorResponse(ex.getMessage(), 400), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {BindException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(BindException ex) {
        return new ResponseEntity<>(ResponseData.errorResponse(ex.getMessage(), 400), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {AccessDeniedException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleExceptionAccessDenied() {
        return new ResponseEntity<>(
                ResponseData.errorResponse(MessageService.getMessage("FORBIDDEN_EXCEPTION"), RestConstants.ACCESS_DENIED), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = {MissingPathVariableException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleExceptionNotFound() {
        return new ResponseEntity<>(
                ResponseData.errorResponse(MessageService.getMessage("PATH_NOTFOUND_EXCEPTION"), RestConstants.NOT_FOUND), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {NoHandlerFoundException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(NoHandlerFoundException ex) {
        return new ResponseEntity<>(
                ResponseData.errorResponse(ex.getMessage(), 404), HttpStatus.NOT_FOUND);
    }

    //METHOD XATO BO'LSA
    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException() {
        return new ResponseEntity<>(
                ResponseData.errorResponse("Method error", 405), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(value = {HttpMediaTypeNotAcceptableException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleExceptionHttpMediaTypeNotAcceptable() {
        return new ResponseEntity<>(
                ResponseData.errorResponse("No acceptable", 406),
                HttpStatus.NOT_ACCEPTABLE);
    }

    //METHOD XATO BO'LSA
    @ExceptionHandler(value = {HttpMediaTypeNotSupportedException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleExceptionHttpMediaTypeNotSupported() {
        return new ResponseEntity<>(ResponseData.errorResponse(MessageService.getMessage("UNSUPPORTED_MEDIA_TYPE"), 415), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(value = {ConversionNotSupportedException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(ConversionNotSupportedException ex) {
        return new ResponseEntity<>(ResponseData.errorResponse(ex.getMessage(), RestConstants.SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {HttpMessageNotWritableException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(HttpMessageNotWritableException ex) {
        return new ResponseEntity<>(
                ResponseData.errorResponse(ex.getMessage(), RestConstants.SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(Exception ex) {
        log.error("EXCEPTION_HELPER:", ex);
        ex.printStackTrace();
        return new ResponseEntity<>(
                ResponseData.errorResponse(
                        MessageService.getMessage("ERROR_IN_SERVER"),
                        RestConstants.SERVER_ERROR),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {AsyncRequestTimeoutException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(AsyncRequestTimeoutException ex) {
        ex.printStackTrace();
        return new ResponseEntity<>(ResponseData.errorResponse(ex.getMessage(), 503), HttpStatus.SERVICE_UNAVAILABLE);
    }

    // FOYDALANUVCHI TOMONIDAN XATO SODIR BO'LGANDA
    @ExceptionHandler(value = {RestException.class})
    public ResponseEntity<ResponseData<ErrorData>> handleException(RestException ex) {
        ex.printStackTrace();

        //AGAR RESOURSE TOPILMAGANLIGI XATOSI BO'LSA CLIENTGA QAYSI TABLEDA NIMA TOPILMAGANLIGI HAQIDA XABAR QAYTADI
        if (ex.getFieldName() != null)
            return new ResponseEntity<>(ResponseData.errorResponse(ex.getUserMessage(), ex.getErrorCode()), ex.getStatus());
        //AKS HOLDA DOIMIY EXCEPTIONLAR ISHLAYVERADI
        if (ex.getErrors() != null)
            return new ResponseEntity<>(ResponseData.errorResponse(ex.getErrors()), ex.getStatus());
        return new ResponseEntity<>(ResponseData.errorResponse(ex.getUserMessage(), ex.getErrorCode() != null ? ex.getErrorCode() : ex.getStatus().value()), ex.getStatus());
    }


}
