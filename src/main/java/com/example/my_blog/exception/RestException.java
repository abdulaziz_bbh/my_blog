package com.example.my_blog.exception;

import com.example.my_blog.response.ErrorData;
import com.example.my_blog.utils.RestConstants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class RestException extends RuntimeException {

    private String userMessage;
    private HttpStatus status;

    private String resourceName;
    private String fieldName;
    private Object fieldValue;
    private List<ErrorData> errors;
    private Integer errorCode;

    public RestException(String resourceName, String fieldName, Object fieldValue, String userMessage) {
        super(String.format("%s not found with %s : '%s'", resourceName, fieldName, fieldName));
        this.resourceName = resourceName;
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
        this.userMessage = userMessage;
        this.status = HttpStatus.BAD_REQUEST;
        this.errorCode = RestConstants.NO_ITEMS_FOUND;
    }

    public RestException(String userMessage, int errorCode, HttpStatus status) {
        super(userMessage);
        this.errors = Collections.singletonList(new ErrorData(userMessage, errorCode));
        this.userMessage = userMessage;
        this.status = status;
    }

    public RestException(String userMessage, HttpStatus status) {
        super(userMessage);
        this.userMessage = userMessage;
        this.status = status;
    }

    public static RestException restThrow(String userMessage, HttpStatus httpStatus) {
        return new RestException(userMessage, httpStatus);
    }

    public static RestException restThrow(String userMessage, int errorCode, HttpStatus httpStatus) {
        return new RestException(userMessage, errorCode, httpStatus);
    }
}
