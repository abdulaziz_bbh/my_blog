package com.example.my_blog.entity.reg;

import com.example.my_blog.entity.BaseEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Collection;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Privilege extends BaseEntity {

    String name;

    @ManyToMany(mappedBy = "privileges")
    Collection<Role> roles;

    public Privilege(String name) {
        this.name = name;
    }
}
