package com.example.my_blog.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collections;
import java.util.List;

/**
 * API dan clientga boradigan har qanday success va error response larni qolibga keltirib qaytaruvchi sinf
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseData<T> {

    private Boolean success = false;
    private String message;
    private T data;
    private List<ErrorData> errors;

    /**
     * response with Boolean(success or fail)
     *
     * @param success
     */
    public ResponseData(Boolean success) {
        this.success = success;
    }

    /**
     * response with T and Boolean(data and success or fail)
     *
     * @param data
     * @param success
     */
    public ResponseData(T data, Boolean success) {
        this.data = data;
        this.success = success;
    }

    /**
     * success response with data and message
     *
     * @param data
     * @param success
     * @param message
     */
    public ResponseData(T data, Boolean success, String message) {
        this.data = data;
        this.success = success;
        this.message = message;
    }

    /**
     * response with success or fail and message
     *
     * @param success
     * @param message
     */
    public ResponseData(Boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    /**
     * error response with message and error code
     *
     * @param message
     * @param errorCode
     */
    public ResponseData(String message, Integer errorCode) {
        this.success = Boolean.FALSE;
        this.errors = Collections.singletonList(new ErrorData(message, errorCode));
    }

    /**
     * error response with error data list
     *
     * @param errors
     */
    public ResponseData(List<ErrorData> errors) {
        this.success = Boolean.FALSE;
        this.errors = errors;
    }

    public static <E> ResponseData<E> successResponse(E data) {
        return new ResponseData<>(data, true);
    }

    public static <E> ResponseData<E> successResponse(E data, String message) {
        return new ResponseData<>(data, true, message);
    }

    public static <E> ResponseData<E> successResponse() {
        return new ResponseData<>(true);
    }

    public static ResponseData<String> successResponse(String message) {
        return new ResponseData<>(true, message);
    }

    public static ResponseData<ErrorData> errorResponse(List<ErrorData> errors) {
        return new ResponseData<>(errors);
    }

    public static ResponseData<ErrorData> errorResponse(String message, Integer errorCode) {
        return new ResponseData<>(message, errorCode);
    }
}
