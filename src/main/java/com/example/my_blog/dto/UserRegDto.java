package com.example.my_blog.dto;

import com.example.my_blog.valid.PasswordMatches;
import com.example.my_blog.valid.ValidEmail;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@PasswordMatches
public class UserRegDto {

    @NotNull
    @NotEmpty
    String firstName;

    @NotNull
    @NotEmpty
    String lastName;

    @NotNull
    @NotEmpty
    String password;
    String matchingPassword;

    @NotNull
    @NotEmpty
    @ValidEmail
    String email;


}
