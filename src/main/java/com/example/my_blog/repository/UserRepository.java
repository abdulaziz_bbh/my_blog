package com.example.my_blog.repository;

import com.example.my_blog.entity.reg.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
