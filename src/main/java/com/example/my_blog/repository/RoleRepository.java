package com.example.my_blog.repository;

import com.example.my_blog.entity.reg.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}
